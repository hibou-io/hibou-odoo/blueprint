FROM registry.gitlab.com/hibou-io/hibou-odoo/suite:14.0

COPY --chown=104 ./src/addons /opt/odoo/addons
COPY ./var/conf/odoo/odoo.conf.sample /etc/odoo/odoo.conf

